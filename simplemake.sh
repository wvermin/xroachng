#!/bin/sh
# -copyright-
#-# 
#-# Copyright (C) 2021 Willem Vermin, Original copyright 1991 by J. T. Anderson
#-# 

# This is a script which compiles xroachng.
# Use and adapt this if the 
#   ./configure; make; make install
# suite does not work on your system
#

# Compilers:

# C compiler to compile .c sources:
CC=gcc    

# compile and link flags

FLAGS="-O2"
# if you have pkg-config working for gtk3:
FLAGS="$FLAGS `pkg-config --cflags --libs gtk+-3.0`"
# NOTE: on my system, pkg-config expands to:
# -pthread -I/usr/include/gtk-3.0 -I/usr/include/at-spi2-atk/2.0 -I/usr/include/at-spi-2.0 -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -I/usr/include/gtk-3.0 -I/usr/include/gio-unix-2.0 -I/usr/include/cairo -I/usr/include/pango-1.0 -I/usr/include/fribidi -I/usr/include/harfbuzz -I/usr/include/atk-1.0 -I/usr/include/cairo -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16 -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/libmount -I/usr/include/blkid -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -lgtk-3 -lgdk-3 -lpangocairo-1.0 -lpango-1.0 -lharfbuzz -latk-1.0 -lcairo-gobject -lcairo -lgdk_pixbuf-2.0 -lgio-2.0 -lgobject-2.0 -lglib-2.0


# if you have pkg-config working for gmodule-2.0:
#FLAGS="$FLAGS `pkg-config --cflags --libs gmodule-2.0`"
# NOTE: on my system, pkg-config expands to:
# -pthread -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -Wl,--export-dynamic -lgmodule-2.0 -pthread -lglib-2.0

# if you have pkg-config working for these: x11 xpm 
FLAGS="$FLAGS `pkg-config --cflags --libs x11 xt xpm xext`"
# NOTE: on my system, pkg-config expands to:
# -lXt -lXpm -lX11 -lXext

# link flags for libmath:
FLAGS="$FLAGS -lm"

# following is needed by gtk3 to recognize the buttons:
# (Should be delivered by pkg-config --cflags --libs gmodule-2.0)
FLAGS="$FLAGS -Wl,--export-dynamic"
# or:
# FLAGS="$FLAGS -rdynamic"

version=`./getversion`
if [ "x$version" = x ]; then
   version="Unknown"
fi

FLAGS="$FLAGS -DVERSION=\"$version\""

cd src || exit 1
echo "removing .o files :"
rm -f *.o

echo "creating ui_xml.h :"
./gen_ui_xml.sh  || exit 1

echo "compiling C sources:"
$CC -c *.c $FLAGS || exit 1

echo "creating xroachng in directory $PWD:"
$CC -o xroachng *.o $FLAGS || exit 1

cd ..
echo "creating man page xroachng.1 in directory $PWD:"
version=`src/xroachng -v|awk '{print $2}'`
sed "s'SYSTEMTHEMES'$PKGDATADIR/xroachng/themes/*';s/VERSION/$version/;s/DATE/`date +'%B %Y'`/" < xroachng.1.tmpl > xroachng.1

echo
echo " ***********************************************************************"
echo " ** It seems that you compiled xroachng successfully.                 **"
echo " ** You can try to run it:                                            **"
echo " **                                                                   **"
echo " **    src/xroachng                                                   **"
echo " **                                                                   **"
echo " ** If xroachng works satisfactorily, you can install it:             **"
echo " **   Copy src/xroachng to for example  /usr/local/bin/               **"
echo " **                                                                   **"
echo " ** Optionally, you can install the man page too:                     **"
echo " **   Copy src/xroachng.1 to for example /usr/local/share/man/man1/   **"
echo " **                                                                   **"
echo " ** Optionally, you can install the desktop file and icon:            **"
echo " **   Copy src/xroachng.desktop to for example                        **"
echo " **                 /usr/local/share/applications/                    **"
echo " **   Copy src/xroachng.png to for example                            **"
echo " **                 /usr/local/share/pixmaps/                         **"
echo " ***********************************************************************"

