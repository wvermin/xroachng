/* -copyright-
#-# 
#-# Copyright (C) 2021 Willem Vermin, Original copyright 1991 by J. T. Anderson
#-# 
*/

#include "roach000.xbm"
#include "roach015.xbm"
#include "roach030.xbm"
#include "roach045.xbm"
#include "roach060.xbm"
#include "roach075.xbm"
#include "roach090.xbm"
#include "roach105.xbm"
#include "roach120.xbm"
#include "roach135.xbm"
#include "roach150.xbm"
#include "roach165.xbm"
#include "roach180.xbm"
#include "roach195.xbm"
#include "roach210.xbm"
#include "roach225.xbm"
#include "roach240.xbm"
#include "roach255.xbm"
#include "roach270.xbm"
#include "roach285.xbm"
#include "roach300.xbm"
#include "roach315.xbm"
#include "roach330.xbm"
#include "roach345.xbm"

#include "squish.xbm"

#define ROACH_HEADINGS 24    /* number of orientations */
#define ROACH_ANGLE    15    /* angle between orientations */

typedef struct RoachMap {
    unsigned char *roachBits;
    Pixmap         pixmap;
    int            width;
    int            height;
    float          sine;
    float          cosine;
} RoachMap;

RoachMap roachPix[] = {
    {roach000_bits, None, roach000_width, roach000_height, 0.0, 0.0},
    {roach015_bits, None, roach015_width, roach015_height, 0.0, 0.0},
    {roach030_bits, None, roach030_width, roach030_height, 0.0, 0.0},
    {roach045_bits, None, roach045_width, roach045_height, 0.0, 0.0},
    {roach060_bits, None, roach060_width, roach060_height, 0.0, 0.0},
    {roach075_bits, None, roach075_width, roach075_height, 0.0, 0.0},
    {roach090_bits, None, roach090_width, roach090_height, 0.0, 0.0},
    {roach105_bits, None, roach105_width, roach105_height, 0.0, 0.0},
    {roach120_bits, None, roach120_width, roach120_height, 0.0, 0.0},
    {roach135_bits, None, roach135_width, roach135_height, 0.0, 0.0},
    {roach150_bits, None, roach150_width, roach150_height, 0.0, 0.0},
    {roach165_bits, None, roach165_width, roach165_height, 0.0, 0.0},
    {roach180_bits, None, roach180_width, roach180_height, 0.0, 0.0},
    {roach195_bits, None, roach195_width, roach195_height, 0.0, 0.0},
    {roach210_bits, None, roach210_width, roach210_height, 0.0, 0.0},
    {roach225_bits, None, roach225_width, roach225_height, 0.0, 0.0},
    {roach240_bits, None, roach240_width, roach240_height, 0.0, 0.0},
    {roach255_bits, None, roach255_width, roach255_height, 0.0, 0.0},
    {roach270_bits, None, roach270_width, roach270_height, 0.0, 0.0},
    {roach285_bits, None, roach285_width, roach285_height, 0.0, 0.0},
    {roach300_bits, None, roach300_width, roach300_height, 0.0, 0.0},
    {roach315_bits, None, roach315_width, roach315_height, 0.0, 0.0},
    {roach330_bits, None, roach330_width, roach330_height, 0.0, 0.0},
    {roach345_bits, None, roach345_width, roach345_height, 0.0, 0.0},
};


