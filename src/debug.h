/* -copyright-
#-# 
#-# Copyright (C) 2021 Willem Vermin, Original copyright 1991 by J. T. Anderson
#-# 
*/
// Macros P, R and I can be used as printf, but they attach the location of the
// statement to the output
// P and R for debugging
// P : only active when DEBUG is defined
// R : always active
// I : meant for production
#pragma once
#include <stdio.h>
#ifdef DEBUG
#define P(...) do {printf ("%s: %d: ",__FILE__,__LINE__);printf(__VA_ARGS__);fflush(stdout);}while(0)
#else
#define P(...) {}
#endif
#define R(...) do {printf ("%s: %d: ",__FILE__,__LINE__);printf(__VA_ARGS__);fflush(stdout);} while(0)
#define I(...) do {printf ("Rroachng info: %s: %d: ",__FILE__,__LINE__);printf(__VA_ARGS__);fflush(stdout);} while(0)

extern int counter;
