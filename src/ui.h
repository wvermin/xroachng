/* -copyright-
#-# 
#-# Copyright (C) 2021 Willem Vermin, Original copyright 1991 by J. T. Anderson
#-# 
 */

extern void ui(void);
extern void set_buttons(void);
extern void WriteFlags(void);
extern void ReadFlags(void);
extern void show_dialog(int type, const char *format, const char *text);
extern void iconify(void);

