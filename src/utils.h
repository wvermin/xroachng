/* -copyright-
#-# 
#-# Copyright (C) 2021 Willem Vermin, Original copyright 1991 by J. T. Anderson
#-# 
*/
#pragma once

//#define add_to_mainloop(prio,time,func,datap) g_timeout_add_full(prio,(int)1000*(time),(GSourceFunc)func,datap,0)


#include <stdio.h>
#include <X11/Intrinsic.h>
#include <gtk/gtk.h>
#include <stdlib.h>
#include <math.h>

#define myexp10f(x) (expf(2.3025850930f*x))
#define mylog10f(x) (0.4342944819f*logf(x))

extern float   fsignf(float x);
extern FILE   *HomeOpen(const char *file,const char *mode,char **path);
extern float   sq2(float x, float y);
extern float   sq3(float x, float y, float z);
extern void    my_cairo_paint_with_alpha(cairo_t *cr, double alpha);
extern int     RandInt(int maxint);
extern Window  Window_With_Name( Display *dpy, Window top, const char *name);
extern double  wallclock(void);
extern double  wallcl(void);
extern void    mystrncpy(char *dest, const char *src, size_t n);
extern void    rgba2color(GdkRGBA *c, char **s);

extern Pixel   Black, White;

extern int   is_little_endian(void);
extern int   sgnf(float x);
extern float fsgnf(float x);
extern void  ClearScreen(void);
