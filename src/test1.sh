#!/bin/sh
# -copyright-
#-# 
#-# Copyright (C) 2021 Willem Vermin, Original copyright 1991 by J. T. Anderson
#-# 

EXE=xroachng
if [ -x ./xroachng ]; then
   EXE=./xroachng
fi
# test if 'xroachng -h' more or less works:
$EXE -h | grep -q -i roachcolor 
if [ $? -ne 0 ] ; then
   echo "Error in executing: $EXE -h"
   exit 1
fi
