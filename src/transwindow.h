/* -copyright-
#-# 
#-# Copyright (C) 2021 Willem Vermin, Original copyright 1991 by J. T. Anderson
#-# 
*/
#include <gtk/gtk.h>
#include <X11/Intrinsic.h>

#pragma once

extern int make_trans_window(GtkWidget *Transwindow, int Fullscreen, int sticky, int below, int dock,
      int clickthrough, GdkWindow **gdk_window, Window *x11_window);
void setbelow(GtkWindow *w);
void setabove(GtkWindow *w);
