/* -copyright-
#-# 
#-# Copyright (C) 2021 Willem Vermin, Original copyright 1991 by J. T. Anderson
#-# 
*/
#pragma once
#include <X11/Xlib.h>
#define _(string) (char *)(string)
#define FLAGSFILE ".xroachngrc"
extern Display *display;
extern Window   roachWin;
extern int      maxRoaches;
extern float    roachSpeed;
extern int      FlagsChanged;
extern char    *gutsColor;
extern char    *roachColor;
extern int      counter;
extern int      squishRoach;


extern void roaches_set_number(int n);
extern void setgcColor(void);
extern void setgutsgcColor(void);
extern void set_defaults(void);
extern void handle_squish(int value);
