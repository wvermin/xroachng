/* -copyright-
#-# 
#-# Copyright (C) 2021 Willem Vermin, Original copyright 1991 by J. T. Anderson
#-# 
*/

#include <gtk/gtk.h>
#include <math.h>
#include "ui_xml.h"
#include "ui.h"
#include "debug.h"
#include "xroachng.h"
#include "utils.h"

#include "xroachng.xpm"
#include "squish.xpm"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef __cplusplus
#define MODULE_EXPORT extern "C" G_MODULE_EXPORT
#else
#define MODULE_EXPORT G_MODULE_EXPORT
#endif



static int    human_action = 1;   /* is it a human who is playing with the buttons? */

#define NTYPES 16 
static GtkBuilder      *builder;
static GtkWidget       *hauptfenster;
static GtkStyleContext *hauptfenstersc;
static char             buf[100];
static GtkWidget       *id_nroaches;
static GtkWidget       *id_nroachesvalue;
static GtkWidget       *id_speed;
static GtkWidget       *id_squish;
static GtkWidget       *id_logo1;
static GtkWidget       *id_logo2;
static GtkWidget       *id_logo3;
static GtkWidget       *id_roach_color;
static GtkWidget       *id_guts_color;
static GtkWidget       *id_speedvalue;
static GtkWidget       *id_squish_picture;

static void   handle_css(void);
static void   init_ids(void);
static void   init_pixmaps(void);
static FILE  *openflagsfile(char *mode);
static float  scale_to_speedfactor(float scale);
static void   ab(float Max, float Min, float *a, float *b);
static float  nroaches_to_scale(float n);
static float  speedfactor_to_scale(float speed);
static float  scale_to_nroaches(float scale);


// Sometimes it is good to have a logarithmic scale, such that
//
//  V = a*M*10**s + b 
//
//  where:       V = parameter (e.g. SpeedFactor)
//               M = desired maximum of V (e.g. 4.0)
//               s = value of the gtkscale (0 .. 1.0)
//  Furthermore: m = desired minimum of V (e.g. 0.2)
//
//  Then:
//     a = (M - m)/(9*M)
//     b = m - a*M
//
// The placement of a is a logical choice, the placement of b is 
// more or less random. I need a constant next to a, because I want
// to define minimum V AND maximum V. 
//
// Given V, compute s:
//
//   s = log10((V-b)/(a*M))
// 

void ab(float Max, float Min, float *a, float *b)
{
   *a = (Max - Min)/(9.0*Max);
   *b = Min - (*a)*Max;
}

const float MaxSpeedFactor = 50;
const float MinSpeedFactor = 1;

const float MaxNroaches  = 200;
const float MinNroaches  = 1.0;

void handle_css()
{
}


void show_dialog(int type, const char *format, const char *text)
{
   GtkMessageType message_type;
   if (type == 1)
      message_type = GTK_MESSAGE_ERROR;
   else
      message_type = GTK_MESSAGE_INFO;

   GtkWidget *m = gtk_message_dialog_new(GTK_WINDOW(hauptfenster),
	 GTK_DIALOG_MODAL,
	 message_type,
	 GTK_BUTTONS_OK,
	 format,
	 text
	 );
   g_signal_connect(m,"response",G_CALLBACK(gtk_main_quit),NULL);
   gtk_widget_show_all(m);
}

void init_ids()
{
   id_nroaches       = GTK_WIDGET(gtk_builder_get_object(builder, "id-nroaches"));
   id_nroachesvalue  = GTK_WIDGET(gtk_builder_get_object(builder, "id-nroachesvalue"));
   id_logo1          = GTK_WIDGET(gtk_builder_get_object(builder, "id-logo1"));
   id_logo2          = GTK_WIDGET(gtk_builder_get_object(builder, "id-logo2"));
   id_logo3          = GTK_WIDGET(gtk_builder_get_object(builder, "id-logo3"));
   id_roach_color    = GTK_WIDGET(gtk_builder_get_object(builder, "id-roach-color"));
   id_guts_color     = GTK_WIDGET(gtk_builder_get_object(builder, "id-guts-color"));
   id_speed          = GTK_WIDGET(gtk_builder_get_object(builder, "id-speed"));
   id_squish         = GTK_WIDGET(gtk_builder_get_object(builder, "id-squish"));
   id_speedvalue     = GTK_WIDGET(gtk_builder_get_object(builder, "id-speedvalue"));
   id_squish_picture = GTK_WIDGET(gtk_builder_get_object(builder, "id-squish-picture"));

}

void init_pixmaps()
{
   GdkPixbuf *pixbuf, *pixbuf1;
   pixbuf  = gdk_pixbuf_new_from_xpm_data ((const char **)xroachng);
   pixbuf1 = gdk_pixbuf_scale_simple(pixbuf,64,64,GDK_INTERP_BILINEAR);
   gtk_image_set_from_pixbuf(GTK_IMAGE(id_logo1),pixbuf1);
   gtk_image_set_from_pixbuf(GTK_IMAGE(id_logo2),pixbuf1);
   gtk_image_set_from_pixbuf(GTK_IMAGE(id_logo3),pixbuf1);

   g_object_unref(pixbuf);
   g_object_unref(pixbuf1);

   pixbuf  = gdk_pixbuf_new_from_xpm_data ((const char **)squish_pic);
   pixbuf1 = gdk_pixbuf_scale_simple(pixbuf,64,64,GDK_INTERP_BILINEAR);
   gtk_image_set_from_pixbuf(GTK_IMAGE(id_squish_picture),pixbuf1);

   g_object_unref(pixbuf);
   g_object_unref(pixbuf1);
}

void set_buttons()
{

   int h = human_action;
   human_action = 0;

   gtk_range_set_value(GTK_RANGE(id_nroaches), nroaches_to_scale(maxRoaches));
   sprintf(buf,"%d",maxRoaches);
   gtk_label_set_text(GTK_LABEL(id_nroachesvalue),buf);

   gtk_range_set_value(GTK_RANGE(id_speed),speedfactor_to_scale(roachSpeed));
   sprintf(buf,"%d",(int)(roachSpeed));
   gtk_label_set_text(GTK_LABEL(id_speedvalue),buf);

   GdkRGBA color;
   P("color: %s\n",roachColor);
   gdk_rgba_parse(&color, roachColor);
   gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(id_roach_color),&color);

   gdk_rgba_parse(&color, gutsColor);
   gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(id_guts_color),&color);

   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(id_squish),squishRoach);

   human_action = h;

   FlagsChanged = 1;
}


MODULE_EXPORT void button_nroaches(GtkWidget *w)
{
   if (!human_action)
      return;
   float value = gtk_range_get_value(GTK_RANGE(w));
   P("nroaches: %f\n",value);
   roaches_set_number(scale_to_nroaches(value));
   FlagsChanged = 1;

   sprintf(buf,"%d",maxRoaches);
   gtk_label_set_text(GTK_LABEL(id_nroachesvalue),buf);
}

MODULE_EXPORT void button_speed(GtkWidget *w)
{
   if (!human_action)
      return;
   float value = gtk_range_get_value(GTK_RANGE(w));
   roachSpeed = scale_to_speedfactor(value);
   P("speed: %f %f\n",value,roachSpeed);
   FlagsChanged = 1;

   sprintf(buf,"%d",(int)(roachSpeed));
   gtk_label_set_text(GTK_LABEL(id_speedvalue),buf);
}


MODULE_EXPORT void button_defaults()
{
   ClearScreen();
   set_defaults();
   roaches_set_number(maxRoaches);
   setgcColor();
   setgutsgcColor();
   if (!human_action)
      return;
   set_buttons();
}

MODULE_EXPORT void button_squish(GtkWidget *w)
{
   if (!human_action)
      return;
   int value = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w));
   P("squish: %d\n",value);
   handle_squish(value);
   FlagsChanged = 1;
}

MODULE_EXPORT void button_iconify()
{
   if (!human_action)
      return;
   gtk_window_iconify(GTK_WINDOW(hauptfenster));
}

MODULE_EXPORT void button_roach_color(GtkWidget *w)
{
   GdkRGBA color;
   if (!human_action)
      return;
   gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(w),&color);
   free(roachColor);
   rgba2color(&color,&roachColor);
   setgcColor();
   FlagsChanged = 1;
}

MODULE_EXPORT void button_guts_color(GtkWidget *w)
{
   GdkRGBA color;
   if (!human_action)
      return;
   gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(w),&color);
   free(gutsColor);
   rgba2color(&color,&gutsColor);
   setgutsgcColor();
   FlagsChanged = 1;
}

MODULE_EXPORT void button_squish_color(GtkWidget *w)
{
   GdkRGBA color;
   if (!human_action)
      return;
   gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(w),&color);
   free(gutsColor);
   rgba2color(&color,&gutsColor);
}

FILE *openflagsfile(char *mode)
{
   char *h = getenv("HOME");
   if (h == NULL)
      return NULL;
   char *flagsfile = (char*)malloc((strlen(h)+strlen(FLAGSFILE)+2)*sizeof(char));
   flagsfile[0] = 0;
   strcat(flagsfile,h);
   strcat(flagsfile,"/");
   strcat(flagsfile,FLAGSFILE);
   FILE *f = fopen(flagsfile,mode);
   P("openflagsfile %s %s\n",flagsfile,mode);
   free(flagsfile);
   return f;
}

void ReadFlags()
{
   FILE *f = openflagsfile(_("r"));
   if (f == NULL)
   {
      I("Cannot read $HOME/%s\n",FLAGSFILE);
      return;
   }
   int lineno     = 1;
   while(1)
   {
      char *line = NULL;
      size_t n = 0;
      int m = getline(&line,&n,f);
      if (m<0)
	 break;
      P("ReadFlags: %d [%s]\n",lineno,line);
      char *flag = (char*)malloc((strlen(line)+1)*sizeof(char));
      m = sscanf(line, "%s", flag);
      if (m == EOF || m == 0)
	 continue;
      char *rest = line + strlen(flag);

      char *p;
      p = rest;
      while (*p == ' ' || *p == '\t' || *p == '\n')
	 p++;
      rest = p;
      p = &line[strlen(line)-1];
      while (*p == ' ' || *p == '\t' || *p == '\n')
	 p--;
      *(p+1) = 0;

      P("ReadFlags: %s [%s]\n",flag,rest);
      if(!strcmp(flag,"-roaches"))
      {
	 maxRoaches = atoi(rest);
	 P("roaches: %d\n",maxRoaches);
      }
      else if(!strcmp(flag,"-speed"))
      {
	 roachSpeed = atoi(rest);
	 P("speed: %d\n",(int)roachSpeed);
      }
      else if(!strcmp(flag,"-rc"))
      {
	 free(roachColor);
	 roachColor = strdup(rest);
	 P("roachcolor: %s\n",roachColor);
      }
      else if(!strcmp(flag,"-rgc"))
      {
	 free(gutsColor);
	 gutsColor = strdup(rest);
	 P("gutscolor: %s\n",gutsColor);
      }
      else if(!strcmp(flag,"-squish"))
      {
	 squishRoach = atoi(rest);
	 P("squishRoach: %d\n",squishRoach);
      }
      lineno++;
      free(line);
      free(flag);
   }
   fclose(f);
}

void WriteFlags()
{
   FILE *f = openflagsfile(_("w"));
   if (f == NULL)
   {
      I("Cannot write $HOME/%s\n",FLAGSFILE);
      return;
   }
   fprintf(f, "-roaches %d\n", maxRoaches);
   fprintf(f, "-speed   %d\n", (int)(roachSpeed));
   fprintf(f, "-rc      %s\n", roachColor);
   fprintf(f, "-rgc     %s\n", gutsColor);
   fprintf(f, "-squish  %d\n", squishRoach);
   fclose(f);
}

float speedfactor_to_scale(float speed)
{
   float a,b;
   ab(MaxSpeedFactor, MinSpeedFactor,&a,&b);
   return mylog10f((speed-b)/(a*MaxSpeedFactor));
}

float scale_to_speedfactor(float scale)
{
   float a,b;
   ab(MaxSpeedFactor, MinSpeedFactor,&a,&b);
   return a * MaxSpeedFactor*myexp10f(scale) + b; 
}

float nroaches_to_scale(float n)
{
   float a,b;
   ab(MaxNroaches, MinNroaches, &a, &b);
   return mylog10f((n-b)/(a*MaxNroaches));
}

float scale_to_nroaches(float scale)
{
   float a,b;
   ab(MaxNroaches, MinNroaches, &a, &b);
   return a * MaxNroaches*myexp10f(scale) + b; 
}

void iconify()
{
   gtk_window_iconify(GTK_WINDOW(hauptfenster));
}

void ui()
{
   P("here is uiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii\n");

   builder = gtk_builder_new_from_string (xroachng_xml, -1);
   gtk_builder_connect_signals (builder, builder);
   hauptfenster  = GTK_WIDGET(gtk_builder_get_object(builder, "hauptfenster"));

   hauptfenstersc  = gtk_widget_get_style_context(hauptfenster);

   handle_css();
   char wtitle[100];
   wtitle[0] = 0;
   strcat(wtitle,"XroachnG");
#ifdef HAVE_CONFIG_H
   strcat(wtitle,"-");
   strncat(wtitle,VERSION,99 - strlen(wtitle));
#endif
   gtk_window_set_title(GTK_WINDOW(hauptfenster),wtitle);
   gtk_window_set_resizable(GTK_WINDOW(hauptfenster),False);
   gtk_widget_show_all (hauptfenster);
   g_signal_connect (GTK_WINDOW(hauptfenster), "delete-event", G_CALLBACK (gtk_main_quit), NULL);
   g_signal_connect (GTK_WINDOW(hauptfenster), "destroy", G_CALLBACK (gtk_main_quit), NULL);

   init_ids();

   init_pixmaps();
   set_buttons();
}

