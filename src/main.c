/* -copyright-
#-# 
#-# Copyright (C) 2021 Willem Vermin, Original copyright 1991 by J. T. Anderson
#-# 
*/
/*
   Xroach - A game of skill.  Try to find the roaches under your windows.

   Copyright 1991 by J.T. Anderson

   jta@locus.com

   This program may be freely distributed provided that all
   copyright notices are retained.

   To run:
   xroachng -speed 2 -squish -rc brown -rgc yellowgreen

   Dedicated to Greg McFarlane (gregm@otc.otca.oz.au).

   Squish option contributed by Rick Petkiewizc (rick@locus.com).

   Virtual root code adapted from patch sent by Colin Rafferty who
   borrowed it from Tom LaStrange. Several other folks sent similar
   fixes.

   Some glitches removed by patch from Guus Sliepen (guus@sliepen.warande.net)
   in 2001 (see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=102668#5).

   Last update: 2018-JUL-10
   */

/* @(#)xroach.c	1.5 4/2/91 11:53:31 */

#include <gtk/gtk.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <math.h>
#include <ctype.h>

#include "debug.h"
#include "transwindow.h"
#include "wmctrl.h"
#include "utils.h"
#include "ui.h"
#include "xroachng.h"
#ifndef VERSION
#ifdef HAVE_CONFIG_H
#include <config.h>
#else
#define VERSION "<unknown version>"
#endif
#endif

char Copyright[] = "Xroach\nCopyright 1991 J.T. Anderson";

#include "images/roachmap.h"

typedef unsigned long Pixel;
typedef int ErrorHandler();

#define SCAMPER_EVENT (LASTEvent + 1)

#if !defined(GRAB_SERVER)
#define GRAB_SERVER    0
#endif

static char         *display_name = NULL;
static GC           gc;
static GC           gutsgc;
static int          screen;
static Pixel        black;
static unsigned int display_height;
static unsigned int display_width;
static GtkWidget   *gtkwin;

static int    errorVal     = 0;
static int    eventBlock   = 0;
static int    xroach_trans = 0;
static int    ShowMenu     = 1; 
static Pixmap squishMap;

typedef struct Roach
{
   RoachMap    *rp;
   int          index;
   float        x;
   float        y;
   int          intX;
   int          intY;
   int          steps;
   unsigned int hidden   :1;
   unsigned int turnLeft :1;
   unsigned int clear    :1;
   unsigned int squished :1;
} Roach;

static Roach *roaches = NULL;
static int   curRoaches = 0;
static float turnSpeed  = 10.0;

static Region rootVisible = NULL;

static void   Usage(void);
static void   SigHandler(int);
static Window FindRootWindow(void);
static int    RoachInRect(Roach *roach, int rx, int ry, int x, int y, unsigned int width, unsigned int height);
static int    RoachOverRect(Roach *roach, int rx, int ry, int x, int y, unsigned int width, unsigned int height);
static void   AddRoach(void);
static void   TurnRoach(Roach *roach);
static void   MoveRoach(int rx);
static void   DrawRoaches(void);
static int    RoachErrors(Display *d,XErrorEvent *err);
static int    CalcRootVisible(void);
static int    MarkHiddenRoaches(void);
static Pixel  AllocNamedColor(char *colorName, Pixel dfltPix);
static int    checkSquish(int x, int y);
static int    do_move_draw(void *);
static int    do_check_ev(void *);
static int    do_testing(void *);
static int    do_mouse(void *);
static void   thanks(void);
static int    do_write_flags(void *);

static int    needCalc;

int      maxRoaches;
float    roachSpeed;
Display *display;
Window   roachWin     = 0;
Window   RootWindow   = 0;
char    *gutsColor    = 0;
char    *roachColor   = 0;
int      squishRoach  = 0;
int      counter      = 0;
int      FlagsChanged = 1;

int main(int ac, char *av[])
{
   char                 *arg;
   float                angle;
   RoachMap             *rp;
   XGCValues            xgcv;


   srand48(time(NULL));
   setenv("GDK_BACKEND","x11",1);

   set_defaults();
   for (int ax = 1; ax < ac; ax++)
   {
      if (strcmp(av[ax], "-h") ==0)
	 Usage();
      exit(0);
   }

   gtk_init(&ac, &av);
   ReadFlags();
   /*
      Process command line options.
      */
   for (int ax = 1; ax < ac; ax++)
   {
      arg = av[ax];

      if (strcmp(arg, "-display") == 0)
      {
	 if (++ax == ac)
	    Usage();
	 display_name = av[ax];
      }
      else if (strcmp(arg, "-rc") == 0)
      {
	 if (++ax == ac)
	    Usage();
	 if(roachColor)
	    free(roachColor);
	 roachColor = strdup(av[ax]);
      }
      else if (strcmp(arg, "-speed") == 0)
      {
	 if (++ax == ac)
	    Usage();
	 roachSpeed = (float) strtod(av[ax], (char **) NULL);
      }
      else if (strcmp(arg, "-roaches") == 0)
      {
	 if (++ax == ac)
	    Usage();
	 maxRoaches = (int) strtol(av[ax], (char **) NULL, 0);
      }
      else if (strcmp(arg, "-squish") == 0)
	 squishRoach = True;
      else if (strcmp(arg, "-nomenu") == 0)
	 ShowMenu = 0;
      else if (strcmp(arg, "-rgc") == 0)
      {
	 if (++ax == ac)
	    Usage();
	 if(gutsColor)
	    free(gutsColor);
	 gutsColor = strdup(av[ax]);
      }
      else if (strcmp(arg, "-id") == 0)
      {
	 if (++ax == ac)
	    Usage();
	 roachWin = strtol(av[ax], (char **)NULL, 0);
      }
      else if (strcmp(arg, "-v") == 0)
      {
	 printf("xroachng_version: " VERSION "\n ");
	 exit(0);
      }
      else if (strcmp(arg, "-defaults") == 0)
      {
	 set_defaults();
      }
      else
	 Usage();
   }

   printf("xroachng version " VERSION " starting ...\n");
   P("roachColor: %s\n",roachColor);
   P("gutsColor: %s\n",gutsColor);
   /* Compensate rate of turning for speed of movement. */
   turnSpeed = 200 / roachSpeed;

   if (turnSpeed < 1)
      turnSpeed = 1;

   srand((unsigned int) time((time_t *) NULL));

   /*
      Catch some signals so we can erase any visible roaches.
      */
   signal(SIGKILL, SigHandler);
   signal(SIGINT, SigHandler);
   signal(SIGTERM, SigHandler);
   signal(SIGHUP, SigHandler);

   display = XOpenDisplay(display_name);

   if (display == NULL)
   {
      if (display_name == NULL)
	 display_name = getenv("DISPLAY");

      fprintf(stderr,
	    "%s: cannot connect to X server %s\n",
	    av[0],
	    display_name ? display_name : "(default)");
      exit(1);
   }

   screen  = DefaultScreen(display);
   RootWindow = DefaultRootWindow(display);
   if (!roachWin)
      roachWin = FindRootWindow();

   if(xroach_trans)
      printf("Using a transparent click-through window for the roaches: %#lx\n",roachWin);
   else
   {
      printf("Using existing window for the roaches: %#lx\n",roachWin);
      gtkwin = NULL;
   }

   ClearScreen();

   black   = BlackPixel(display, screen);

   display_width  = (unsigned int) DisplayWidth(display, screen);
   display_height = (unsigned int) DisplayHeight(display, screen);

   /*
      Create roach pixmaps at several orientations.
      */
   for (int ax = 0; ax < 360; ax += ROACH_ANGLE)
   {
      int rx = ax / ROACH_ANGLE;
      angle = (float) (rx * 0.261799387799);
      rp = &roachPix[rx];
      rp->pixmap = XCreateBitmapFromData(display,
	    roachWin,
	    (char*)rp->roachBits,
	    (unsigned int) rp->width,
	    (unsigned int) rp->height);
      rp->sine   = sinf(angle);
      rp->cosine = cosf(angle);
   }

   /*
      Create the squished pixmap
      */
   squishMap = XCreateBitmapFromData(display,
	 roachWin,
	 (char *)squish_bits,
	 squish_width,
	 squish_height);


   gc = XCreateGC(display, roachWin, 0L, &xgcv);
   P("color: %s,#%06lx\n",roachColor,AllocNamedColor(roachColor,black));
   setgcColor();
   XSetFillStyle(display, gc, FillStippled);

   gutsgc = XCreateGC(display, roachWin, 0L, &xgcv);
   setgutsgcColor();
   XSetFillStyle(display, gutsgc, FillStippled);

   roaches_set_number(maxRoaches);

   XSelectInput(display, RootWindow, ExposureMask | SubstructureNotifyMask );


   needCalc = 1;

   g_timeout_add_full(G_PRIORITY_DEFAULT, 20,   do_move_draw ,NULL,NULL);
   g_timeout_add_full(G_PRIORITY_DEFAULT, 200,  do_check_ev ,NULL,NULL);
   g_timeout_add_full(G_PRIORITY_DEFAULT, 400,  do_write_flags ,NULL,NULL);
   g_timeout_add_full(G_PRIORITY_DEFAULT, 20,   do_testing ,NULL,NULL);
   g_timeout_add_full(G_PRIORITY_DEFAULT, 40,   do_mouse ,NULL,NULL);

   ui();

   if (!ShowMenu)
      iconify();
   gtk_main();

   ClearScreen();
   XCloseDisplay(display);
   thanks();
   return 0;
}

void setgcColor()
{
   GdkRGBA rgba;
   if (!gdk_rgba_parse(&rgba,roachColor))
      roachColor = _("black");
   XSetForeground(display, gc, AllocNamedColor(roachColor, black)|0xff000000);
}

void setgutsgcColor()
{
   GdkRGBA rgba;
   if (!gdk_rgba_parse(&rgba,gutsColor))
      gutsColor = _("yellowgreen");
   XSetForeground(display, gutsgc, AllocNamedColor(gutsColor, black)|0xff000000);
}

void set_defaults()
{
   roachSpeed  = 15;
   maxRoaches  = 20;
   squishRoach = 0;
   if (roachColor)
      free(roachColor);
   roachColor = strdup("black");
   if (gutsColor)
      free(gutsColor);
   gutsColor = strdup("yellowgreen");
}

int do_move_draw(void *dummy)
{
   (void)dummy;
   for (int rx = 0; rx < curRoaches; rx++)
      if (!roaches[rx].hidden && !roaches[rx].squished)
	 MoveRoach(rx);

   DrawRoaches();
   P("%d Drawroaches ...\n",counter++);
   XFlush(display);
   XSync(display, False);
   return TRUE;
}

int do_testing(void *dummy)
{
   (void)dummy;
   return False;

   Window root_return, child_return;
   int x,y,wx,wy;
   unsigned int mask;
   XQueryPointer(display, roachWin, &root_return, &child_return, &x, &y, &wx, &wy, &mask);
   if (mask == 0x100)
      P("do_testing: %d %#lx %#lx %d %d %d %d %#x\n",counter++,root_return,child_return,x,y,wx,wy,mask);
   return True;
}

int do_mouse(void *dummy)
{
   if (!squishRoach)
      return True;
   (void)dummy;

   Window root_return, child_return;
   int x,y,wx,wy;
   unsigned int mask;
   XQueryPointer(display, roachWin, &root_return, &child_return, &x, &y, &wx, &wy, &mask);
   if (mask == 0x100)
   {
      P("do_mouse: %d %#lx %#lx %d %d %d %d %#x\n",counter++,root_return,child_return,x,y,wx,wy,mask);
      checkSquish(wx,wy);
   }
   return True;
}

int do_check_ev(void * dummy)
{
   (void)dummy;
   XEvent               ev;
   int                  nVis;
   (void)dummy;
   P("%d do_check_ev\n",counter++);

   while (XPending(display))
   {
      XNextEvent(display, &ev);
      switch (ev.type)
      {
	 case UnmapNotify:
	    P("unmapnotify\n");
	    needCalc = 1;
	    break;

	 case MapNotify:
	 case Expose:
	 case ConfigureNotify:
	    needCalc = 1;
	    break;
	 default:
	    break;
      }
   }

   {
      P("%d needCalc %d\n",counter++,needCalc);
      if (needCalc)
	 needCalc = CalcRootVisible();

      if (needCalc)
	 nVis = 0;
      else
	 nVis = MarkHiddenRoaches();

      ev.type = SCAMPER_EVENT;
      (void)nVis;
   }

   return TRUE;
}



#define USEPRT(msg) printf(msg)

void Usage()
{
   USEPRT("Usage: xroachng [options]\n\n");
   USEPRT("Options:\n");
   USEPRT("       -display displayname\n");
   USEPRT("       -rc      roachcolor\n");
   USEPRT("       -roaches numroaches\n");
   USEPRT("       -speed   roachspeed\n");
   USEPRT("       -squish\n");
   USEPRT("       -rgc     roachgutscolor\n");
   USEPRT("       -nomenu\n");
   USEPRT("       -defaults\n");
   USEPRT("       -v\n");

   exit(1);
}

void SigHandler(int s)
{
   /*
      If we are blocked, no roaches are visible and we can just bail
      out.  If we are not blocked, then let the main procedure clean
      up the root window.
      */
   if (eventBlock)
   {
      XCloseDisplay(display);
      exit(0);
   }
   else
   {
      gtk_main_quit();
   }
   (void)s;
}

/*
   Find the root or virtual root window, more precise: find a window to draw in.
   */
Window FindRootWindow()
{
   P("in FindRootWIndow\n");
   Window        searchWin;

   Window Rootwindow = DefaultRootWindow(display);
   Window root;
   int x,y;
   unsigned int w,h,b,depth;
   XGetGeometry(display,Rootwindow,&root,
	 &x, &y, &w, &h, &b, &depth);

   gtkwin = gtk_window_new(GTK_WINDOW_TOPLEVEL); 
   gtk_window_set_title             (GTK_WINDOW(gtkwin),"Xroachng-A");
   gtk_window_set_skip_taskbar_hint (GTK_WINDOW(gtkwin),TRUE);
   gtk_window_set_skip_pager_hint   (GTK_WINDOW(gtkwin),TRUE);
   int HaveTrans = make_trans_window(gtkwin,
	 1 /*fullscreen*/,
	 1 /*sticky*/,
	 1 /* below*/,
	 1 /* clickthrough */,
	 1 /* dock*/ , 
	 NULL,
	 &searchWin);

   if (HaveTrans)
   {
      xroach_trans = 1;
   }
   else
   {
      searchWin = Rootwindow;
      // Maybe, it is LXDE: find window with name pcmanfm
      char *DesktopSession = NULL;
      if (getenv("DESKTOP_SESSION"))
      {
	 DesktopSession = strdup(getenv("DESKTOP_SESSION"));
	 char *a = DesktopSession;
	 while (*a) { *a = toupper(*a); a++; }
	 if (!strncmp(DesktopSession,"LXDE",4)) 
	 {
	    Window w = Window_With_Name(display, Rootwindow, "pcmanfm");
	    if(w)
	    {
	       searchWin = w;
	       printf("LXDE session found, using window pcmanfm\n");
	    }
	 }
      }
      if(DesktopSession)
	 free(DesktopSession);
   }

   if(xroach_trans)
   {
      XMoveWindow(display,searchWin,0,0);
      XSetWindowBackground(display,searchWin,0);
   }

   P("searchWin: %#lx\n",searchWin);
   return searchWin;
}


/*
   Check for roach completely in specified rectangle.
   */
int RoachInRect(Roach *roach, int rx, int ry, int x, int y, unsigned int width, unsigned int height)
{
   if (rx < x)
      return 0;

   if ((rx + roach->rp->width) > (int)(x + width))
      return 0;

   if (ry < y)
      return 0;

   if ((ry + roach->rp->height) > (int)(y + height))
      return 0;

   return 1;
}

/*
   Check for roach overlapping specified rectangle.
   */
int RoachOverRect(Roach *roach, int rx, int ry, int x, int y, unsigned int width, unsigned int height)
{
   if (rx >= (int)(x + width))
      return 0;

   if ((rx + roach->rp->width) <= x)
      return 0;

   if (ry >= (int)(y + height))
      return 0;

   if ((ry + roach->rp->height) <= y)
      return 0;

   return 1;
}

/*
   Give birth to a roach.
   */
void AddRoach()
{
   Roach *r;

   if (curRoaches < maxRoaches)
   {
      r           = &roaches[curRoaches++];
      r->index    = RandInt(ROACH_HEADINGS);
      r->rp       = &roachPix[r->index];
      r->x        = RandInt(display_width - r->rp->width);
      r->y        = RandInt(display_height - r->rp->height);
      r->intX     = -1;
      r->intY     = -1;
      r->hidden   = 0;
      r->steps    = RandInt((int) turnSpeed);
      r->turnLeft = RandInt(100) >= 50;
      r->clear    = 0;
      r->squished = 0;
   }
}

/*
   Turn a roach.
   */
void TurnRoach(Roach *roach)
{
   if (roach->index != (roach->rp - roachPix))
      return;

   if (roach->turnLeft)
   {
      roach->index += (RandInt(30) / 10) + 1;

      if (roach->index >= ROACH_HEADINGS)
	 roach->index -= ROACH_HEADINGS;
   }
   else
   {
      roach->index -= (RandInt(30) / 10) + 1;

      if (roach->index < 0)
	 roach->index += ROACH_HEADINGS;
   }
}

/*
   Move a roach.
   */
void MoveRoach(int rx)
{
   float newX;
   float newY;
   Roach *roach;

   roach = &roaches[rx];
   newX = roach->x + (roachSpeed * roach->rp->cosine);
   newY = roach->y - (roachSpeed * roach->rp->sine);

   if (RoachInRect(roach,
	    (int) newX, (int) newY,
	    0, 0,
	    display_width, display_height))
   {
      roach->x = newX;
      roach->y = newY;

      if (roach->steps-- <= 0)
      {
	 TurnRoach(roach);
	 roach->steps = RandInt((int) turnSpeed);

	 /*
	    Previously, roaches would just go around in circles.
	    This makes their movement more interesting (and disgusting too!).
	    */
	 if (RandInt(100) >= 80)
	    roach->turnLeft ^= 1;
      }

      (void) RoachOverRect;
      /* This is a kind of anti-collision algorithm which doesn't do what it is supposed to do,
	 it eats CPU time and sometimes makes roaches spin around very crazy. Therefore it is
	 commented out.

	 for (int ii = rx + 2; ii < curRoaches; ii++) {
	 r2 = &roaches[ii];

	 if (RoachOverRect(roach,
	 (int) newX, (int) newY,
	 r2->intX, r2->intY,
	 (unsigned int) r2->rp->width, (unsigned int) r2->rp->height))
	 TurnRoach(roach);

	 } */
   }
   else
   {
      TurnRoach(roach);
   }
}

/*
   Draw all roaches.
   */
void DrawRoaches()
{
   Roach *roach;
   // it seems that, when the ui is showing a tooltip, the background is gone
   // for the transparent window created with create_transparent_window() ...
   // I do not understand, in xsnow there is no such thing ...
   // So, we define the background again here:
   if(xroach_trans)
      XSetWindowBackground(display,roachWin,0);

   for (int rx = 0; rx < curRoaches; rx++)
   {
      roach = &roaches[rx];

      if(!roach->clear)
      {
	 P("%d clear %d %d %d %d\n",counter++,roach->intX,roach->intY,roach->rp->width,roach->rp->height);
	 XClearArea(display,
	       roachWin,
	       roach->intX,
	       roach->intY,
	       (unsigned int) roach->rp->width,
	       (unsigned int) roach->rp->height,
	       False);
	 roach->clear = 1;
      }
   }
   for (int rx = 0; rx < curRoaches; rx++)
   {
      roach = &roaches[rx];
      GC mygc;

      if (!roach->hidden)
      {
	 roach->intX = (int) roach->x;
	 roach->intY = (int) roach->y;
	 if (roach->squished)
	 {
	    mygc      = gutsgc;
	 }
	 else
	 {
	    roach->rp = &roachPix[roach->index];
	    mygc      = gc;
	 }

	 P("%#lx %d %d %d %d\n",roachWin,roach->intX,roach->intY,roach->rp->width,roach->rp->height);
	 if(1)
	 {
	    if(roach->squished)
	       XSetStipple(display, mygc, squishMap);
	    else
	       XSetStipple(display,  mygc, roach->rp->pixmap);
	    XSetTSOrigin(display, mygc, roach->intX, roach->intY);
	 }
	 else
	 {
	    if(roach->squished)
	       XSetClipMask(display, mygc, squishMap);
	    else
	       XSetClipMask(display, mygc, roach->rp->pixmap);
	    XSetClipOrigin(display, mygc, roach->intX,roach->intY);
	 }
	 XFillRectangle(display,
	       roachWin,
	       mygc,
	       roach->intX,
	       roach->intY,
	       (unsigned int) roach->rp->width,
	       (unsigned int) roach->rp->height);
	 roach->clear = 0;
      }
      else
      {
	 roach->intX = -1;
	 roach->clear = 0;
      }
      if (roach->squished)
	 roach->clear = 1; // a squished roach does not move: no need to clear.
   }
}

#if !GRAB_SERVER
int RoachErrors(Display *d,XErrorEvent *err)
{
   (void)d;
   errorVal = err->error_code;

   return 0;
}
#endif /* GRAB_SERVER */

/*
   Calculate visible region of root window.
   */
int CalcRootVisible()
{
   Region            covered;
   Region            visible;
   XRectangle        rect;

   /*
      If we don't grab the server, the XGetWindowAttribute or XGetGeometry
      calls can abort us.  On the other hand, the server grabs can make for
      some annoying delays.
      */
#if GRAB_SERVER
   XGrabServer(display);
#else
   XSetErrorHandler(RoachErrors);
#endif

   /*
      Get children of root.
      */
   //XQueryTree(display, RootWindow, &dummy, &dummy, &children, &nChildren);
   WinInfo *windows;
   int nwin;
   GetWindows(&windows,&nwin);
   P("%d nwin %d\n",counter++,nwin);

   /*
      For each mapped child, add the window rectangle to the covered
      region.
      */
   covered = XCreateRegion();

   for (int wx = 0; wx < nwin; wx++)
   {
      int x,y,width,height;
      if (XEventsQueued(display, QueuedAlready))
      {
	 XDestroyRegion(covered);
	 P("%d already queued\n",counter++);
	 return 1;
      }

      WinInfo *win = &windows[wx];

      if (win->id == roachWin)
	 continue;
      if (win->w == display_width)
	 continue;
      if(!win->hidden && !win->dock)
      {
	 x      = win->x;
	 y      = win->y;
	 width  = win->w;
	 height = win->h;
	 /* Entirely offscreen? */
	 if (x >= (int)display_width)     continue;
	 if (y >= (int)display_height)    continue;
	 if (y <= 0)                 continue;
	 if (x + width < 0)          continue; 

	 rect.x = x;
	 rect.y = y;

	 rect.width = width;
	 rect.height = height;

	 P("%d rect: %d %d %d %d\n",counter++,rect.x,rect.y,rect.width,rect.height);

	 XUnionRectWithRegion(&rect, covered, covered);
      }
   }

   free(windows);

#if GRAB_SERVER
   XUngrabServer(display);
#else
   XSetErrorHandler(NULL);
#endif

   /*
      Subtract the covered region from the root window region.
      */
   visible = XCreateRegion();

   rect.x = 0;
   rect.y = 0;

   rect.width  = (unsigned short) display_width;
   rect.height = (unsigned short) display_height;

   XUnionRectWithRegion(&rect, visible, visible);
   XSubtractRegion(visible, covered, visible);
   XDestroyRegion(covered);

   /*
      Save visible region globally.
      */
   if (rootVisible)
      XDestroyRegion(rootVisible);

   rootVisible = visible;

   /*
      Mark all roaches visible.
      */
   for (int wx = 0; wx < curRoaches; wx++)
   {
      roaches[wx].hidden = 0;
      roaches[wx].clear  = 0;
   }

   return 0;
}


/*
   Mark hidden roaches.
   */
int MarkHiddenRoaches()
{
   int   nVisible;
   Roach *r;

   nVisible = 0;

   for (int rx = 0; rx < curRoaches; rx++)
   {
      r = &roaches[rx];

      if (!r->hidden)
      {
	 if (r->intX > 0 && XRectInRegion(rootVisible,
		  r->intX,
		  r->intY,
		  (unsigned int) r->rp->width,
		  (unsigned int) r->rp->height) == RectangleOut)
	    r->hidden = 1;
	 else
	    nVisible++;
      }
   }

   return nVisible;
}

/*
   Allocate a color by name.
   */
Pixel AllocNamedColor(char *colorName, Pixel dfltPix)
{
   Pixel  pix;
   XColor exactcolor;
   XColor scrncolor;

   if (XAllocNamedColor(display,
	    DefaultColormap(display, screen),
	    colorName,
	    &scrncolor,
	    &exactcolor))
      pix = scrncolor.pixel;
   else
      pix = dfltPix;

   return pix;
}

/*
 * Check to see if we have to squish any roaches.
 */
int checkSquish(int x, int y)
{
   Roach *r;
   int rc = 0;

   for (int rx = 0; rx < curRoaches; rx++)
   {
      r = &roaches[rx];

      if(r->squished)
	 continue;

      if(r->hidden)
	 continue;

      if (r->rp == NULL)
	 continue;

      if (x > r->intX && x < (r->intX + r->rp->width) && y > r->intY && y < (r->intY + r->rp->height))
      {
	 rc++;
	 XSetStipple(display, gutsgc, squishMap);
	 XSetTSOrigin(display, gutsgc, r->intX, r->intY);
	 P("checkSquish: %d %#lx %d %d\n",rx,roachWin,squish_width,squish_height);
	 r->squished = 1;
      }
   }
   return rc;
}

void roaches_set_number(int n)
{
   P("roaches_set_number: %d\n",n);
   roaches = (Roach *) realloc(roaches,sizeof(Roach) * n);
   maxRoaches = n;
   curRoaches = 0;
   ClearScreen();
   while (curRoaches < maxRoaches)
      AddRoach();
}

void thanks()
{
   printf("\nThank you for using xroachng\n");
}

int do_write_flags(void *dummy)
{
   if (FlagsChanged)
   {
      FlagsChanged = 0;
      WriteFlags();
   }
   return TRUE;
   (void)dummy;
}

void handle_squish(int value)
{
   P("handle_squish %d\n",value);
   squishRoach = value;
   if (!squishRoach)
      for(int i=0; i<curRoaches; i++)
	 roaches[i].squished = 0;
   return;
}

