# xroachng
The classic xroach game for X11: displays disgusting cockroaches on your root
window. These creepy crawlies scamper around until they find a window to
hide under. Whenever you move or iconify a window, the exposed beetles again
scamper for cover.

Adapted for desktop environments like Gnome and KDE. Still works for FVWM, TWM
and the like.

## Build
```
$ tar xf xroachng-x.y.z.tar.gz
$ cd xroachng-x.y.z
$ ./configure
$ make
$ sudo make install
```

## Install Debian package
### amd64 architecture (64 bits)
```
$ sudo apt install ./xroachng_x.y.z-1_amd64.deb
```
### i386 architecture (32 bits):
```
$ sudo apt install ./xroachng_x.y.z-1_i386.deb
```
### armhf architecture (32 bits) (Raspberry PI):
```
$ sudo apt install ./xroachng_x.y.z-1_armhf.deb
```

## Run
```
$ xroachng -speed 2 -squish -rc brown -rgc yellowgreen
```

## Copyright
Original copyright 1991 by J. T. Anderson. Squish option contributed by
Rick Petkiewizc. Virtual root code adapted from patch sent by Colin
Rafferty who borrowed it from Tom LaStrange. Several other folks sent
similar fixes. Some glitches removed by patch from Guus Sliepen.

Willem Vermin adapted Xroach for Gnome, KFE etc. and added a graphical 
user interface.
