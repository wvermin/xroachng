#!/bin/sh
# -copyright-
#-# 
#-# Copyright (C) 2021 Willem Vermin, Original copyright 1991 by J. T. Anderson
#-# 
crfile=`mktemp`
cat << eof | sed 's/^/#-# /' > "$crfile"

Copyright (C) 2021 Willem Vermin, Original copyright 1991 by J. T. Anderson

eof
n=0
while [ "$1" ] ; do
   f="$1"
   shift
   txt="-""copyright-"
   if file --mime "$f" | grep -q binary ; then
      echo "$f: binary"
      continue
   fi
   if ! grep -q -- "$txt" "$f" ; then 
      echo "$f: no $txt"
      continue
   fi
   # following 2 sed commands take care that only the first '-copyright-'
   # will be followed by the copyright text
   # Notice that sed requires a newline after the filename of the 'r' command
   sed -i "/^\s*#-#/d" "$f"
   sed -i "/$txt/{r $crfile
   :a;n;ba;}" "$f"
   n=`expr $n + 1`
done
echo "$n files copyrighted"
